

//Function with parameter
function printName(name){
	console.log("My name is " + name)
}
printName("MEOW")
printName("Glyn")


function greeting(){
	console.log("Hello, User!")
}


greeting("Eric")


//Using Multiple Parameters

function createFullName(firstname, middlename, lastname){
	console.log(firstname+ " " + middlename + " " + lastname)
}

createFullName("Glyn","De Asis","Maulani")


function checkDivisibilityBy8(num){
	let remainder = num % 8
	console.log("The remainder of " + num + " divided by 8 is " + remainder)
	let isDivisibleBy8 = (remainder === 0)
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8)


}
checkDivisibilityBy8(64)
checkDivisibilityBy8(28)

/*

	Mini-Activity:
		1. Create a function which is able to receive data as an argument
			-This function should be able to receive the name of your favorite superhero
			-Display the name your favourite superhero

*/

function displaySuperhero(superhero){
	console.log("My favourite superhero is: " + superhero)
}
displaySuperhero("ALL MIGHT")


//Return Statement

function returnFullName(firstName,middleName,lastName){
	return firstName + " " + middleName + " " + lastName
	console.log("Can we print this message?")
}



let displayFullName = returnFullName("John","Doe","Smith")
console.log(displayFullName)


/*
	Mini-Activity: 
		1.Debug our code. So that the function will be able to return a value and save it in a variable


*/

function printPlayerInformation(userName,level,job){
	return "Username: " + userName + "\n " + "Level: " + level + "\n " +  "Job: " +job
}

let user1 = printPlayerInformation("cardo123", "999","Mcdo Cashier")

console.log(user1)




