


function addNumbers(num1,num2){
	let result = num1 + num2
	console.log("The sum of " + num1 + " and " + num2 + " is: " + result)
}

addNumbers(5,15)

function subtractNumbers(num1,num2){
	let result = num1 - num2
	console.log("The difference of " + num1 + " and " + num2 + " is: " + result)
}
subtractNumbers(20,5)

function multiplyNumbers(num1,num2){
	console.log("The product of " +num1 + " and " + num2 + " is :")
	return num1 * num2
}

let product = multiplyNumbers(50,10)
console.log(product)


function divideNumbers(num1,num2){
	console.log("The quotient of " + num1 + " and " + num2 + " is :")
	return num1 / num2
}
let quotient = divideNumbers(50,10)
console.log(quotient)


function getAreaOfCircle(radius){
	console.log("The result of getting the area of a circle with " + radius + " is: ")
	return 3.14 * Math.pow(radius,2)
}

let circleArea = getAreaOfCircle(15)
console.log(circleArea)


function getAverageOfFourNumbers(num1,num2,num3,num4){
	console.log("The average of " + num1 + ", " + num2 + ", " +num3 + ", "+ num4 +" is :")
	return (num1 + num2 + num3 + num4) / 4
}
let averageVar = getAverageOfFourNumbers(20,40,60,80)
console.log(averageVar)


function isPassed(score,total){
	console.log("Is "+ score + "/"+total+ " a passing score?")
	return ((score/total) * 100) >= 75
}
let isPassingScore = isPassed(38,50)
console.log(isPassingScore)

